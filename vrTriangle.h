#pragma once
#include "vrShape.h"
#include "vrBase/vrTypes.h"
namespace VR
{
	class vrTriangle : public vrShape
	{
	public:
		vrTriangle()
		{
			id = Invalid_Id;
			material_id = Invalid_Id;
		}
		~vrTriangle();

	public:
		vrInt   id;
		vrInt   material_id;
		vrVec3I vtx_indices;
		vrVec3I nml_indices;
		vrVec3I tex_indices;
	};
}