#include <GL/glew.h>
#include <GL/freeglut.h>

#include "vrBase/vrTypes.h"
#include "vrModel.h"
#include <vector>

#include "vrBase/vrRotation.h"
extern VR::Interactive::vrBallController g_trackball_1;
extern float g_scene_scale;

#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "tiny_obj_loader/tiny_obj_loader.h"
namespace VR
{
	vrModel g_model_plane;
	vrModel g_model_logo;
	void load_Obj(vrModel& dstModel, const vrString obj_file);
	void showModel(const vrModel& dstModel, const vrVec3 translate = vrVec3(0.0, 0.0, 0.0), const vrFloat scale = 1.0);

	void init_Scene()
	{
		load_Obj(g_model_plane, vrString("mesh/Plane3d.obj"));
		load_Obj(g_model_logo, vrString("mesh/yangchen_name_cn_yahei_cn.obj"));
	}

	void showScene()
	{
		g_trackball_1.IssueGLrotation();
		glScalef(g_scene_scale, g_scene_scale, g_scene_scale);
		glColor3d(103 / 255.0, 46 / 255.0, 59 / 255.0);
		showModel(g_model_logo, vrVec3(0.0,0.0,0.0), 0.3);
		//glutSolidCube(0.3);
	}

	void display(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glColor3d(196 /255.0, 143 / 255.0, 101 / 255.0);
		
		glPushMatrix();
		showModel(g_model_plane, vrVec3(0.0,-1.5,0.0));
		glPopMatrix();

		glPushMatrix();
		VR::showScene();
		glPopMatrix();

		glutSwapBuffers();
	}

	void showModel(const vrModel& dstModel, const vrVec3 translate, const vrFloat scale)
	{
		glScaled(scale, scale, scale);
		glTranslated(translate[0], translate[1], translate[2]);

		glBegin(GL_TRIANGLES);

		for (iterAllOf(itr_tri, dstModel.g_vec_triangles))
		{
			const vrTriangle& curTri = *itr_tri;
			const vrVec3I& vtx_indices = curTri.vtx_indices;
			const vrVec3I& nml_indices = curTri.nml_indices;

			glNormal3dv(dstModel.g_vec_vertex_nml[nml_indices[0]].data());
			glVertex3dv(dstModel.g_vec_vertex_pos[vtx_indices[0]].data());

			glNormal3dv(dstModel.g_vec_vertex_nml[nml_indices[1]].data());
			glVertex3dv(dstModel.g_vec_vertex_pos[vtx_indices[1]].data());

			glNormal3dv(dstModel.g_vec_vertex_nml[nml_indices[2]].data());
			glVertex3dv(dstModel.g_vec_vertex_pos[vtx_indices[2]].data());
		}

		glEnd();
	}

	void load_Obj(vrModel& dstModel, const vrString obj_file)
	{
		dstModel.clear();

		dstModel.name = obj_file;

		std::string inputfile = obj_file;
		tinyobj::attrib_t attrib;
		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;

		std::string err;
		bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, inputfile.c_str());

		if (!err.empty()) { // `err` may contain warning message.
			std::cerr << err << std::endl;
		}

		if (!ret) {
			std::cerr << inputfile << std::endl;
			vrError("tinyobj::LoadObj failure.");
		}

		{
			// vertex position
			
			for (size_t v = 0; v < attrib.vertices.size() / 3; v++)
			{
				dstModel.g_vec_vertex_pos.push_back(vrVec3(
					static_cast<vrFloat>(attrib.vertices[3 * v + 0]),
					static_cast<vrFloat>(attrib.vertices[3 * v + 1]),
					static_cast<vrFloat>(attrib.vertices[3 * v + 2])
				));
				/*printf("  v[%ld] = (%f, %f, %f)\n", static_cast<long>(v),
					static_cast<const double>(attrib.vertices[3 * v + 0]),
					static_cast<const double>(attrib.vertices[3 * v + 1]),
					static_cast<const double>(attrib.vertices[3 * v + 2]));*/
			}
			// normal 
			for (size_t v = 0; v < attrib.normals.size() / 3; v++) 
			{
				dstModel.g_vec_vertex_nml.push_back(vrVec3(
					static_cast<vrFloat>(attrib.normals[3 * v + 0]),
					static_cast<vrFloat>(attrib.normals[3 * v + 1]),
					static_cast<vrFloat>(attrib.normals[3 * v + 2])
				));
				/*printf("  n[%ld] = (%f, %f, %f)\n", static_cast<long>(v),
					static_cast<const double>(attrib.normals[3 * v + 0]),
					static_cast<const double>(attrib.normals[3 * v + 1]),
					static_cast<const double>(attrib.normals[3 * v + 2]));*/
			}
			// texture coordinate
			for (size_t v = 0; v < attrib.texcoords.size() / 2; v++) 
			{
				dstModel.g_vec_vertex_tex.push_back(vrVec2(
					static_cast<vrFloat>(attrib.texcoords[2 * v + 0]),
					static_cast<vrFloat>(attrib.texcoords[2 * v + 1])
				));
				/*printf("  uv[%ld] = (%f, %f)\n", static_cast<long>(v),
					static_cast<const double>(attrib.texcoords[2 * v + 0]),
					static_cast<const double>(attrib.texcoords[2 * v + 1]));*/
			}
		}

		vrASSERT(1 == shapes.size());
		// Loop over shapes
		for (size_t s = 0; s < shapes.size(); s++) {
			// Loop over faces(polygon)
			size_t index_offset = 0;
			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
				int fv = shapes[s].mesh.num_face_vertices[f];
				vrASSERT(3 == fv);

				vrTriangle curTriNode;
				// Loop over vertices in the face.
				for (size_t v = 0; v < fv; v++) {
					// access to vertex
					tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];

					curTriNode.vtx_indices[v] = idx.vertex_index;
					curTriNode.nml_indices[v] = idx.normal_index;
					curTriNode.tex_indices[v] = idx.texcoord_index;

					/*tinyobj::real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
					tinyobj::real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
					tinyobj::real_t vz = attrib.vertices[3 * idx.vertex_index + 2];
					tinyobj::real_t nx = attrib.normals[3 * idx.normal_index + 0];
					tinyobj::real_t ny = attrib.normals[3 * idx.normal_index + 1];
					tinyobj::real_t nz = attrib.normals[3 * idx.normal_index + 2];
					tinyobj::real_t tx = attrib.texcoords[2 * idx.texcoord_index + 0];
					tinyobj::real_t ty = attrib.texcoords[2 * idx.texcoord_index + 1];*/
					// Optional: vertex colors
					// tinyobj::real_t red = attrib.colors[3*idx.vertex_index+0];
					// tinyobj::real_t green = attrib.colors[3*idx.vertex_index+1];
					// tinyobj::real_t blue = attrib.colors[3*idx.vertex_index+2];
				}
				index_offset += fv;

				// per-face material
				shapes[s].mesh.material_ids[f];

				dstModel.g_vec_triangles.push_back(curTriNode);
			}
		}
	}
}
