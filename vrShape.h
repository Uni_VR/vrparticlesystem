#pragma once

namespace VR
{
	class vrShape
	{
	public:
		virtual ~vrShape() = 0;
	};
}