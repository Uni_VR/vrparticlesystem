#pragma once
//#include "vrVertex.h"
#include "vrBase/vrTypes.h"
#include "vrTriangle.h"
#include <vector>
namespace VR
{
	class vrModel
	{
	public:
		void clear()
		{
			name = vrString("");
			g_vec_vertex_pos.clear();
			g_vec_vertex_nml.clear();
			g_vec_vertex_tex.clear();
			g_vec_triangles.clear();
		}
	public:
		vrString name;
		std::vector< vrVec3 > g_vec_vertex_pos;
		std::vector< vrVec3 > g_vec_vertex_nml;
		std::vector< vrVec2 > g_vec_vertex_tex;
		std::vector< vrTriangle > g_vec_triangles;
	};
}