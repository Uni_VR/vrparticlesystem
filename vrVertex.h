#pragma once
#include "vrBase/vrTypes.h"
namespace VR
{
	
	class vrVertex
	{
	public:
		vrVertex()
		{
			id = Invalid_Id;
		}

		~vrVertex()
		{}
	public:
		vrInt  id;
		vrVec3 pos;
		vrVec3 normal;
		vrVec2 texCoord;
	};
}